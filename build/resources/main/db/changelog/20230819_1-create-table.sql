--liquibase formatted sql

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE CARD_MANAGEMENT
(
    ID         uuid PRIMARY KEY default uuid_generate_v4(),
    CARD_NUMBER         varchar(50),
    CIF               varchar(20),
    ACCOUNT         varchar(50),
    PHONE_NUMBER varchar(100),
    EMBOSSING_NAME    varchar(200),
    CARD_LIMIT integer,
    PRODUCT_CODE varchar(20),
    EXPIRED_DATE    timestamp,
    UPDATED_AT timestamp
);