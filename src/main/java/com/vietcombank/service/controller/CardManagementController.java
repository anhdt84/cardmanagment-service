package com.vietcombank.service.controller;

import com.vietcombank.service.entity.CardManagementEntity;
import com.vietcombank.service.service.CardManagementService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;

@Slf4j
@RestController
@RequestMapping("/card-management")
public class CardManagementController {
    @Autowired
    private CardManagementService cardManagementService;

    @GetMapping(value = "/v1/get-all-card-management-info", produces = "application/json")
    @Operation(summary = "Retrieve All Card Management Info")
    public ResponseEntity<?> getAllCardManagementInfo() {
        log.info("Retrieve All Card Management Info");
        return ResponseEntity.ok(cardManagementService.retrieveAll());
    }

    @PostMapping("/v1/save-card-management-info")
    private ResponseEntity<?> saveCardManagementInfo(@RequestBody CardManagementEntity cardManagementEntity) {
        try {
            log.info("Save Card Management Info");
            cardManagementService.save(cardManagementEntity);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/v1/get-card-management-info-by-id/{id}")
    public ResponseEntity<?> getCardManagementInfoById(@PathVariable String id) {
        try {
            log.info("Get Card Management Info By Id");
            CardManagementEntity cardManagementEntity = cardManagementService.getById(id);
            return ResponseEntity.ok(cardManagementEntity);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/v1/update-card-management-info")
    public ResponseEntity<?> updateCardManagementInfo(@RequestBody CardManagementEntity cardManagementEntity) {
        try {
            log.info("Update Card Management Info");
            cardManagementService.save(cardManagementEntity);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/v1/delete-card-management-info/{id}")
    public ResponseEntity<?> deleteCardManagementInfo(@PathVariable String id) {
        try {
            log.info("Delete Card Management Info By Id");
            cardManagementService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
