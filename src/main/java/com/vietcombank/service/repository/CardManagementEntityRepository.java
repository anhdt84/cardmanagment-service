package com.vietcombank.service.repository;

import com.vietcombank.service.entity.CardManagementEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CardManagementEntityRepository extends JpaRepository<CardManagementEntity, UUID> {
}