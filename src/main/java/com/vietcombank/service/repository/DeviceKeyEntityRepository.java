package com.vietcombank.service.repository;

import com.vietcombank.service.entity.DeviceKeyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface DeviceKeyEntityRepository extends JpaRepository<DeviceKeyEntity, UUID> {
}