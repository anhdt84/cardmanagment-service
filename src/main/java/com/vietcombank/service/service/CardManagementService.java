package com.vietcombank.service.service;

import com.vietcombank.service.entity.CardManagementEntity;
import com.vietcombank.service.repository.CardManagementEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class CardManagementService {
    @Autowired
    private CardManagementEntityRepository cardManagementEntityRepository;

    public List<CardManagementEntity> retrieveAll() {
        return cardManagementEntityRepository.findAll();
    }

    public void save(CardManagementEntity cardManagementEntity) {
        cardManagementEntityRepository.save(cardManagementEntity);
    }

    public void delete(String id) {
        cardManagementEntityRepository.deleteById(UUID.fromString(id));
    }

    public CardManagementEntity getById(String id) {
        return cardManagementEntityRepository.findById(UUID.fromString(id)).get();
    }
}
